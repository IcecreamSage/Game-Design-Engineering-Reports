## Scrabbles

###  Game Rule Change

We  introduce a new rule called _Reveal_

*  Every player will have exactly one chance in the entire game to see all the character pieces of the next player

### Premise of the Rule Change

* The game was mostly about increasing one's point before, but after the change of rule, it's also about obstructing next palyer's chances of gaining points
* It brings more competitiveness into the game.

### Change of Gameplay

* Its like killing two birds with one shot, it increases a player's points while simultaneously lowers the chance of next player to gain significant points.
* A Player can force the next player to dispose most of its current pieces and get new ones in order to prevent other players from taking advantage.

#### Game Math

- Interestingly there is as such no math behind the game scrabble. It’s mainly because different scrabble allow you to use different words which could also change with time.The game will certainly get over as there are huge number of words available out there to put on the board.

- Strategy 

  - - Improve the vocabulary before playing the game, know the words like OOGONIUM, SIBILATE, CWM. These words might look like gibberish but are legitimate according to Official SCRABBLE Players Dictionary. 
    - Knowing lot of two-three letter words can help massively as without knowing them, there are high chances of losing the game. These words can get you out of a jam, help you get rid of excess vowels, and give you building blocks for your next turn.
    - For building new words the player can use suffixes and prefixes.
    - Strategically using the blank tile score the maximum points along side the letters J, Q, X, and Z or with Bingo.
    - Exchanging plays one of the most important part in gameplay when absolutely cannot spell a word, or if you really, really just hate your rack. 

#### Player Engagement

* It generates excitement to play a move such that the next player won't be left with much chance to gain points
* It can be a very good comeback strategy for losing player to see the next player's characters and obstruct his victory

#### Convergence of the game

* The no. of cells are limited in scrabble, so the game will eventually reach a state where it won't be possible for any player to make any new word and therefore the game would end. 

### Time to complete the game

* From a losing player's perspective, it can delay the result that player employ this strategy to prevent the next player from making significant amount if points
* From a winning player's perspective, it can result in quicker game as the winning player can see next player's characters and prevent him from staging a comeback 