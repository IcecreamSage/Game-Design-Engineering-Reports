## Mancala

## Game Strategies

We analysed and got to know some basic strategies which help in winning the game. Refer to the image 'mancala.jpg'. We will discuss with respect to player A. Player B is opponent.

- First move : If each pit has 4 stones, then A should play A4, which will end at A's mancala and A will get one more chance. In second chance, A should play A6 which has 5 stones. Thus A wll create situation such that  B can't get two moves in next turn.
- A will try to keep more stones on his part. So if B gets empty then A can take all his stones. This strategy should be practiced with some caution.
- Looping : If A want to capture the stones from B6 and he has much number of stones in A6 and empty pit in A1, then he can play move with A6. If it ends on empty A1 due to large number of stones then A can keep all stones from B6.
- Rushing : There are some configurations in which A ends up in his mancala many times thus getting more and more turns. For example, A5 : 2 and A6 : 1. In this situation, A's moves A6, A5, A6 will lead him getting all three stones in his mancala.
- Stuffing : If B has lots of opportunities to end up in his empty and hence capturing your stones, then A can use his full pit to fill up B's pit to avoid such situation.
