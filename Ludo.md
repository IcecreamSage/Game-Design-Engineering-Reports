## Ludo

### Game Rule Change

We introduced a new rule called _stacking_.
- A player can stack one of his pieces on another one of his pieces if both the pieces reach the same block somehow. Ex. If two pieces of a player are three blocks away from each other and player gets 3 on his dice, then he can move the backward piece 3 blocks ahead and stack it up on the existing piece on the block, if he wishes to.
- After stacking, player can't unstack the pieces. Stack can grow larger or remain the same.
- After stacking, if the player gets $d$ on the dice then, he can move a stack of $n$ pieces $[d / n]$ blocks ahead. Here $[d / n]$ is integer division.
- Stack of $n1$ pieces can kill the stack of $n2$ pieces if $n1 >= n2$.



### Premise of Rule Change

- We wanted to introduce more effective decision making into the game rather than just chance. Here the decision whether to stack the pieces or not, is in player's hand.



### Change in Game Play

- Because of stacking, a player can keep his stack safe from the smaller stacks of opponents. So a player tends to stack his pieces when they come to the end because it reduces the chances of getting killed and solidifies his position.
- If a player stacks at the early stage i.e. just after starting the journey, its pace will get considerably reduced.
- The game with changed rule becomes more interesting when played by many players i.e. 4 players.
- If every player stacks too much then the game will become very slow and boring.

## Game Math

- A piece travels $57$ moves during his journey, if it's not attacked by any other piece. Expected dice value is $3.5$, thus average moves required to go to the end is almost $57 / 3.5 = 16.29$  . Exact value is higher because at the end, if player pass the end block, he has to repeat the end line again.

- With changed rule, if a player stack two pieces then expected move value is

  $$ E = ([1 / 2] + [2 / 2] + [3 / 2] + [4 / 2] + [5 / 2] + [6 / 2])  / 6$$

  $$ E = (0 + 1 + 1 + 2 + 2 + 3)  / 6$$

  $$E = 1.5$$

  Thus, we can say that affective movement is $2 * E = 3$  because two pieces were stacked. Thus due to integer division, effective movement is reduced from $3.5$ to $3$ per dice roll. Hence the game becomes little slow.



#### Player Engagement

- We played with changed rule. We realized that the game now was slightly more strategic and interesting than before. It now involves more decision making.
- It can happen that a player can corner his opponent into stacking at the early stage of the journey, thereby reducing his opponent's pace.

#### Convergence of the game

- After the rule change, the chances for the convergence of the game will remain almost the same as before. Normal Ludo game can go infinite if the players can't get the exact required number on dice. This possibility of infinite game remains even after the rule change.

#### Time to Complete the Game

- It seems that the game pace will remain the same after the rule change because effective piece move after rule change is same almost.
- But since we are taking integer division, there can be less effective piece movement than before because in many cases, $[d / n] * n < d$. 
- It may happen that player can't move at all in his chance.