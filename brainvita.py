import numpy as np
import copy

board_main = [
    [9, 9, 1, 1, 1, 9, 9],
    [9, 9, 1, 1, 1, 9, 9],
    [1, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 0, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 1],
    [9, 9, 1, 1, 1, 9, 9],
    [9, 9, 1, 1, 1, 9, 9]
]

N = 7
dr = [0, 1, 0, -1]
dc = [1, 0, -1, 0]

def find_moves(board):

    avail_moves = []

    for i in xrange(7):
        for j in xrange(7):
            if board[i][j] != 0:
                continue
            for l, m in zip(dr, dc):
                ni = i + 2 * l
                nj = j + 2 * m
                ji = i + l
                jj = j + m
                if ni < 0 or ni >= 7 or nj < 0 or nj >= 7:
                    continue
                if board[ji][jj] != 1:
                    continue
                if board[ni][nj] != 1:
                    continue
                avail_moves.append(((ni, nj), (i, j)))

    return avail_moves


max_iter = 100000
score_freq = []
for i in xrange(33):
    score_freq.append(0)

for it in xrange(max_iter):

    board = copy.deepcopy(board_main)
    moves = 0
    play = []
    while True:
        avail_moves = find_moves(board)
        l = len(avail_moves)
        if l == 0:
            break
        k = np.random.randint(0, l)
        (i, j), (ni, nj) = avail_moves[k]
        play.append(((i, j), (ni, nj)))
        board[i][j] = 0
        board[ni][nj] = 1
        board[(ni + i) / 2][(nj + j) / 2] = 0
        moves += 1

    if 32 - moves == 26:
        print play
        break
    score_freq[32 - moves] += 1

print score_freq
